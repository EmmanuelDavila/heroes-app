import React from "react";

const { shallow, mount } = require("enzyme");
const { AuthContext } = require("../../auth/AuthContext");
const { AppRouter } = require("../../routers/AppRouter");

describe("Pruebas en <Appouter/>", () => {
  const contextValue = {
    dispatch: jest.fn(),
    user: {
      logged: false,
    },
  };

  test("Debe de mostrar el login si no esta autenticado ", () => {
    const wrapper = mount(
      <AuthContext.Provider value={contextValue}>
        <AppRouter />
      </AuthContext.Provider>
    );

    expect(wrapper).toMatchSnapshot();
  });

  test('Debe de mostrar el componente marvel si está autenticado', () => {

    const contextValue = {
        dispatch: jest.fn(),
        user: {
          logged: true,
          name: 'Chadwick'
        },
      };

    const wrapper = mount(
        <AuthContext.Provider value={contextValue}>
          <AppRouter />
        </AuthContext.Provider>
      );
  
      expect(wrapper.find('.navbar').exists()).toBe(true);
    //   console.log(wrapper.html());
  })
  
});
