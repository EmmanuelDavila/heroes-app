const { authReducer } = require("../../auth/authReducer");
const { types } = require("../../types/types");

describe("Pruebas en authReducer", () => {
  test("Debe de retornar el estado por defecto", () => {
    const state = authReducer({ logged: false }, {});
    expect(state).toEqual({ logged: false });
  });

  test("Debe de autenticar y colocar el name del usuario", () => {

    const action = {
        type: types.login,
        payload:{
            name: 'Oz'
        }
    }
    
    const state = authReducer({ logged: false }, action);
    expect(state).toEqual({ logged: true,  name:'Oz'});
  });

  test("Debe de borrar el name del usaurio y logged en false", () => {
    const action = {
        type: types.logout,
        payload:{
            name: ''
        }
    }
    
    const state = authReducer({ logged: false }, action);
    expect(state).toEqual({ logged: false});
  });
});
